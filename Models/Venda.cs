using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace tech_test_payment_api.Models
{
    public class Venda
    {
        public int Id { get; set; }

        [System.Text.Json.Serialization.JsonIgnore]
        public int VendedorId { get; set; }

        public Vendedor Vendedor { get; set; }

        public List<Produto> Produtos { get; set; }

        public EnumStatusVenda StatusDaVenda { get; set; }
        
        public DateTime DataCompra { get; set; }
    }
}