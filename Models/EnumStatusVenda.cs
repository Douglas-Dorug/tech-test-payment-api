using System.Text.Json.Serialization;

namespace tech_test_payment_api.Models
{
    [JsonConverter(typeof(JsonStringEnumConverter))]
    public enum EnumStatusVenda
    {
        Aguardando_pagamento = 0,
        Pagamento_aprovado = 1,
        Enviado_para_transportadora = 2,
        Entregue = 4,
        Cancelada = 5
    }
}