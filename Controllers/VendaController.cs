using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using tech_test_payment_api.Models;
using tech_test_payment_api.Context;

namespace tech_test_payment_api.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class VendaController : ControllerBase
    {

        private readonly VendaContext _context;

        public VendaController(VendaContext vendaContext)
        {
            _context = vendaContext;
        }

        [HttpPost("RegistrarVenda")]
        public IActionResult Criar(Venda venda)
        {
            try{
                if (venda.Produtos.Count() == 0){
                    return BadRequest(new { Erro = "Deve ser informado ao menos um produto para a venda" });
                }

                //Faz a chegagem se o Id do vendedor informado já existe no banco de dados
                //Caso o vendedor informado não exista é retornado um erro.
                if(venda.Vendedor.Id != 0)
                {
                    venda.Vendedor = _context.Vendedor.Find(venda.Vendedor.Id);
                    if(venda.Vendedor == null)
                    {
                        return NotFound("Vendedor informado não existe.");
                    }
                }

                venda.StatusDaVenda = EnumStatusVenda.Aguardando_pagamento; //Atualiza o status da venda para Aguardando Pagamento

                _context.Add(venda);
                _context.SaveChanges();
                return Ok(venda);
            }
            catch(Exception)
            {
                return StatusCode(500, "Ocorreu um problema durante a transação, por favor contatar o administrador.");
            }
        }

        [HttpGet("{id}")]
        public IActionResult ObterPorId(int id)
        {
            try{

                var venda = _context.Vendas.Find(id);
                
                if(venda == null)
                {
                    return NotFound("Esta venda não existe, por favor entrar com um id valido de venda.");
                }
                //Obtem todos os produtos relacionados a venda
                List<Produto> produtos = _context.Produto.Where(X => X.VendaId.Equals(venda.Id)).ToList();
                
                //Obtem as informações do vendedor relacionado a venda
                venda.Vendedor = _context.Vendedor.Find(venda.VendedorId);
                return Ok(venda);

            }
            catch (Exception)
            {
                return StatusCode(500, "Ocorreu um problema durante a transação, por favor contatar o administrador.");
            }
        }

        [HttpPut("AtualizarStatusVenda")]
        public IActionResult AtualizarStatusVenda(int idVenda, EnumStatusVenda enumStatusVenda)
        {
            try
            {
                var vendaBanco = _context.Vendas.Find(idVenda);

                //Validação inicial das informações obtidas
                if(vendaBanco == null)
                    return NotFound("Esta venda não existe, por favor entrar com um id valido de venda");

                if(vendaBanco.StatusDaVenda == enumStatusVenda)
                    return BadRequest("Atualização do pedido não pode ser igual ao estado atual");
                
                //Faz a verificação se os parametros foram passados corretamente para que ocorra a atualização
                //Caso o usuário faça uma requisição invalida será retornado um erro 400 BadRequest
                if(vendaBanco.StatusDaVenda == EnumStatusVenda.Entregue)
                {
                    return BadRequest("O pedido já foi entregue!");
                }
                else if(vendaBanco.StatusDaVenda == EnumStatusVenda.Entregue)
                {
                    return BadRequest("O pedido já foi cancelado!");
                }
                else if(vendaBanco.StatusDaVenda == EnumStatusVenda.Aguardando_pagamento 
                && (enumStatusVenda == EnumStatusVenda.Pagamento_aprovado || enumStatusVenda == EnumStatusVenda.Cancelada))
                {
                    vendaBanco.StatusDaVenda = enumStatusVenda;
                }
                else if(vendaBanco.StatusDaVenda == EnumStatusVenda.Pagamento_aprovado 
                && (enumStatusVenda == EnumStatusVenda.Enviado_para_transportadora || enumStatusVenda == EnumStatusVenda.Cancelada))
                {
                    vendaBanco.StatusDaVenda = enumStatusVenda;
                }
                else if(vendaBanco.StatusDaVenda == EnumStatusVenda.Enviado_para_transportadora && enumStatusVenda == EnumStatusVenda.Entregue)
                {
                    vendaBanco.StatusDaVenda = enumStatusVenda;
                }
                else
                {
                    return BadRequest("Opção informada para atualização de status é invalida.");
                }


                _context.Update(vendaBanco);
                _context.SaveChanges();

                return Ok(vendaBanco.StatusDaVenda);   
            } 
            catch(Exception)
            {
                return StatusCode(500, "Ocorreu um problema durante a transação, por favor contatar o administrador.");
            }
        }
    }
}